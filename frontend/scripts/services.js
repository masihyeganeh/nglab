(function() {
  'use strict';

  /* services */
  angular.module('app.services', []).factory('api', [
    '$resource', function($resource) {
      return {
        exampleCall: function(success, failure) {
          var path;
          path = $resource('/api/v1/list');
          return path.query({}, function(list, rHs) {
            if (angular.isFunction(success)) {
              return success(list);
            }
          }, failure);
        }
      };
    }
  ]);

}).call(this);
