(function() {
  'use strict';
  var App;

  App = angular.module('app', ['ngRoute', 'ngResource', 'ngSanitize', 'ngCookies', 'ui.bootstrap', 'app.controllers', 'app.filters', 'app.directives', 'app.services']).run(
  function($http, $rootScope, $route, CSRF_TOKEN) {
    return $http.defaults.headers.common['csrf_token'] = CSRF_TOKEN;
  }).config([
    '$routeProvider', '$locationProvider', function($routeProvider, $locationProvider, fileUploadProvider) {
      $routeProvider.when('/', {
        controller: 'MainCtrl'
      }).otherwise({
        redirectTo: '/'
      });
      return $locationProvider.html5Mode(true);
    }
  ]);

}).call(this);
