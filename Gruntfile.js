module.exports = function(grunt) {
  grunt.initConfig({
    concat: {
      options: {
        separator: ';'
      },
      vendors: {
        src: [
          './vendor/bower_components/jquery/dist/jquery.js',
          './vendor/bower_components/jquery-migrate/jquery-migrate.js',
          './vendor/bower_components/bootstrap/dist/js/bootstrap.js',
          './vendor/bower_components/bootstrap/dist/js/bootstrap.js',
          './vendor/bower_components/angular/angular.js',
          './vendor/bower_components/angular-route/angular-route.js',
          './vendor/bower_components/angular-resource/angular-resource.js',
          './vendor/bower_components/angular-sanitize/angular-sanitize.js',
          './vendor/bower_components/angular-cookies/angular-cookies.js',
          './vendor/bower_components/angular-i18n/angular-locale_fa-ir.js',
          './vendor/bower_components/consolelog/consolelog.js',
          './vendor/bower_components/angular-bootstrap/ui-bootstrap.js',
          './vendor/bower_components/angular-bootstrap/ui-bootstrap-tpls.js'
        ],
        dest: './public/js/vendors.js'
      },
      scripts: {
        src: [
          './frontend/scripts/functions.js',
          './frontend/scripts/app.js',
          './frontend/scripts/directives.js',
          './frontend/scripts/filters.js',
          './frontend/scripts/services.js',
          './frontend/scripts/controllers.js'
        ],
        dest: './public/js/app.js'
      }
    },
    less: {
      all: {
        options: {
          compress: true
        },
        files: {
          './public/css/app.css': './frontend/styling/app.less',
          './public/css/admin.css': './frontend/styling/admin.less'
        }
      }
    },
    uglify: {
      options: {
        mangle: false  // Use if you want the names of your functions and variables unchanged
      },
      all: {
        files: {
          './public/js/vendors.js': './public/js/vendors.js',
          './public/js/app.js': './public/js/app.js',
          './public/js/templates.js': './frontend/scripts/templates.js',
          './public/js/admin.js': './frontend/scripts/admin.js', // for later use
          './public/js/respond.js': './frontend/scripts/respond.js'
        }
      }
    },
    ngtemplates: {
      app: {
        options: {
          prefix: '/'
        },
        cwd: './app/views/',
        src: 'partials/**.html',
        dest: './public/js/templates.js'
      }
    },
    clean: {
      app: [
        './public/js',
        './public/css',
        './public/fonts',
        './public/img'
      ],
      production: ['./vendor/bower_components', './node_modules']
    },
    copy: {
      templates: {
        files: [
          {
            expand: true,
            src: ['./frontend/templates/**'],
            dest: './app/views/',
            flatten: true,
            filter: 'isFile'
          }
        ]
      },
      main: {
        files: [
          {
            expand: true,
            src: ['./frontend/img/**'],
            dest: './public/img/',
            flatten: true,
            filter: 'isFile'
          }, {
            expand: true,
            src: ['./vendor/bower_components/bootstrap/fonts/**'],
            dest: './public/fonts/',
            flatten: true,
            filter: 'isFile'
          }, {
            expand: true,
            src: ['./frontend/fonts/**'],
            dest: './public/fonts/',
            flatten: true,
            filter: 'isFile'
          }, {
            expand: true,
            src: ['./vendor/bower_components/respond/src/**'],
            dest: './public/js/',
            flatten: true,
            filter: 'isFile'
          }
        ]
      }
    },
    watch: {
      less: {
        files: './frontend/styling/*.less',
        tasks: 'less'
      },
      copy: {
        files: './frontend/img/**',
        tasks: 'copy'
      },
      templates: {
        files: '**/*.html',
        tasks: ['copy:templates', 'ngtemplates']
      },
      scripts: {
        files: './frontend/scripts/*.js',
        tasks: 'concat:scripts'
      }
    }
  });
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-angular-templates');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.registerTask('default', [
    'clean:app',
    'less:all',
    'concat',
    'copy:templates',
    'copy:main',
    'ngtemplates'
  ]);
  return grunt.registerTask('production', [
    'clean:app',
    'less:all',
    'concat',
    'copy:templates',
    'copy:main',
    'ngtemplates',
    'uglify:all',
    'clean:production'
  ]);
};
